﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using oneMoreTime.Models;

namespace oneMoreTime.Controllers
{
    public class DepartmentController : Controller
    {
        private MyDatabaseEntities db = new MyDatabaseEntities();

        // GET: Department
        public ActionResult Index()
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../generated.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("departments");
            XmlNodeList dep = RootNode.ChildNodes;



            return View(dep);
        }

        // GET: Department/Details/5
        public ActionResult Details(int? id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../generated.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("departments");
            XmlNodeList deps = RootNode.ChildNodes;

            XmlNode dep = deps[0];

            foreach (XmlNode d in deps)
            {
                if (d["Id"].InnerText == id.ToString())
                {
                    dep = d;
                }
            }

            return View(dep);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Department/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Department department)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../generated.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("departments");


            XmlNode departmentNode = RootNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "department", ""));

            departmentNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Id", "")).InnerText = department.depId.ToString();
            departmentNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "dName", "")).InnerText = department.depName;
            departmentNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "dHead", "")).InnerText = department.depHead;

            XmlDoc.Save(Server.MapPath("../generated.xml"));


            return View(department);

        }

        // GET: Department/Edit/5
        public ActionResult Edit(String id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../generated.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("departments");
            XmlNodeList deps = RootNode.ChildNodes;

            XmlNode dep = deps[0];

            foreach (XmlNode d in deps)
            {
                if (d["Id"].InnerText == id)
                {
                    dep = d;
                }
            }

            return View(dep);

        }
        // POST: Department/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Department department)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../generated.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("departments");
            XmlNodeList deps = RootNode.ChildNodes;

            XmlNode dep = deps[0];

            foreach (XmlNode d in deps)
            {
                if (d["Id"].InnerText == department.depId.ToString())
                {
                    dep = d;
                }
            }

            dep["dName"].InnerText = department.depName;
            dep["dHead"].InnerText = department.depHead;



            XmlDocObj.Save(Server.MapPath("../generated.xml"));

            return RedirectToAction("Index");

        }
        // GET: Department/Delete/5
        public ActionResult Delete(String id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../generated.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("departments");
            XmlNodeList deps = RootNode.ChildNodes;

            XmlNode dep = deps[0];

            foreach (XmlNode d in deps)
            {
                if (d["Id"].InnerText == id)
                {
                    dep = d;
                }
            }

            return View(dep);
        }

        // POST: Department/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../../generated.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("departments");
            XmlNodeList deps = RootNode.ChildNodes;

            XmlNode dep = deps[0];

            foreach (XmlNode d in deps)
            {
                if (d["Id"].InnerText == id.ToString())
                {
                    dep = d;
                }
            }

            RootNode.RemoveChild(dep);

            XmlDocObj.Save(Server.MapPath("../../generated.xml"));

            return RedirectToAction("Index");
        }


    }
}
