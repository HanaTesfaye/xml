﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Xml.Serialization;
using oneMoreTime.Models;

namespace oneMoreTime
{
    public class XmlManager
    {
        public static void XmlDataWriter(object obj, string filename)
        {
            XmlSerializer sr = new XmlSerializer(obj.GetType());
            TextWriter writer = new StreamWriter(filename);
            sr.Serialize(writer, obj);
            writer.Close();
        }

        //Userdata xml reader
        public static Department XmlDataReader(string filename)
        {
            Department obj = new Department();
            XmlSerializer xs = new XmlSerializer(typeof(Department));
            FileStream reader = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            obj = (Department)xs.Deserialize(reader);
            reader.Close();
            return obj;
        }
    }
}